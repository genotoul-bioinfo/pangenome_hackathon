## *Zymoseptoria tritici*, wheat pathogen

*Zymoseptoria tritici* is a pathogen responsible for Septoria leaf blotch in wheat. The genome of *Z.tritici* is haploid, 40 Mb long and contains 21 chromosomes, 8 of which are accessory (14-21). Several complete genomes are available and have been used to explore the sequence specificity of diverse populations sampled worldwide. Here we propose to work with 8 genomes previously analysed as part of the establishment of a gene pangenome in the *Badet et al, 2020* study. We will focus on chromosome 5, which shows a pattern of presence/absence of a group of genes. The strain TN09 contains a specific gene cluster (Indole Metabolism) with 9 genes (jg9858 to jg9866).

List of genomes and associated files:

| Strain     | Genome                 | Genes                         | TEs                        |
|------------|------------------------|-------------------------------|----------------------------|
| Aus01      | Aus01.chr_5.fa.gz      | Aus01.chr_5.genes.gff.gz      | Aus01.chr_5.TE.gff.gz      |
| IPO323     | IPO323.chr_5.fa.gz     | IPO323.chr_5.genes.gff.gz     | IPO323.chr_5.TE.gff.gz     |
| ST99CH_1A5 | ST99CH_1A5.chr_5.fa.gz | ST99CH_1A5.chr_5.genes.gff.gz | ST99CH_1A5.chr_5.TE.gff.gz |
| ST99CH_1E4 | ST99CH_1E4.chr_5.fa.gz | ST99CH_1E4.chr_5.genes.gff.gz | ST99CH_1E4.chr_5.TE.gff.gz |
| ST99CH_3D1 | ST99CH_3D1.chr_5.fa.gz | ST99CH_3D1.chr_5.genes.gff.gz | ST99CH_3D1.chr_5.TE.gff.gz |
| ST99CH_3D7 | ST99CH_3D7.chr_5.fa.gz | ST99CH_3D7.chr_5.genes.gff.gz | ST99CH_3D7.chr_5.TE.gff.gz |
| TN09       | TN09.chr_5.fa.gz       | TN09.chr_5.genes.gff.gz       | TN09.chr_5.TE.gff.gz       |
| YEQ92      | YEQ92.chr_5.fa.gz      | YEQ92.chr_5.genes.gff.gz      | YEQ92.chr_5.TE.gff.gz      |


We will also work with strains from field populations of *Z.tritici*. These strains were sequenced in short-reads. The data are available under the Bioproject: [PRJNA777581](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA777581)
We perform a selection of subset from several runs for this tutorial:

* [SRR16762562](https://trace.ncbi.nlm.nih.gov/Traces?run=SRR16762562)
* ...


Download all data at once:
```shell

wget https://genotoul-bioinfo.pages.mia.inra.fr/pangenome_hackathon/data/ztritici.tar

# if you want to control md5sum
wget https://genotoul-bioinfo.pages.mia.inra.fr/pangenome_hackathon/data/ztritici.tar.md5
md5sum -c ztritici.tar.md5
```

## References

[1] T. Badet, U. Oggenfuss, L. Abraham, B. A. McDonald, and D. Croll, “A 19-isolate reference-quality global pangenome for the fungal wheat pathogen Zymoseptoria tritici,” BMC Biol., vol. 18, no. 1, Feb. 2020, doi: 10.1186/s12915-020-0744-3.

## Authors

* Nicolas Lapalu (nicolas.lapalu[at]inrae.fr)
