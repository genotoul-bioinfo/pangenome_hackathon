# Pangenome Hackathon

https://genotoul-bioinfo.pages.mia.inra.fr/pangenome_hackathon/

How to Edit
Run the website locally with the following command

    docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material:9.4.4

Open http://localhost:8000 to get a preview.

Now edit/add ``md` files in `docs` dir following [mkdocs](https://www.mkdocs.org/) and [mkdocs-material](https://www.mkdocs.org/) documentations.