# Generated data

Just in case, [here](https://web-genobioinfo.toulouse.inrae.fr/~amergez/Hackathon/hackathon.tar.gz) are the files that will be generated during the hackaton.

To extract the tarball:
```bash
tar -xf hackathon.tar.gz
```
